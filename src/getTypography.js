export const getTypography = (colors) => ({
  fontFamily: [
    'Noto Sans', 'OpenSans-Regular', 'Open Sans', 'sans-serif',
  ].join(','),
  fontSize: 13,
  color: colors.neutrals['text-strong'].value,
  fontWeight: 400,

  h1: {
    fontFamily: [
      'Noto Sans Light', 'OpenSans-Light', 'Open Sans Light', 'Open Sans', 'sans-serif',
    ].join(','),
    fontWeight: 200,
    letterSpacing: '8px',
    fontSize: 14,
    color: colors.neutrals['text-strong'].value,
    textTransform: 'uppercase',
  },
  h2: {
    fontFamily: [
      'Noto Sans Semi Bold', 'OpenSans-Semibold', 'Open Sans Semibold', 'Open Sans', 'sans-serif',
    ].join(','),
    fontSize: 28,
    fontWeight: 650,
    color: colors.neutrals['text-strong'].value,
    textTransform: 'uppercase',
  },
  h3: {
    fontFamily: [
      'Noto Sans Bold', 'OpenSans-Bold', 'Open Sans Bold', 'Open Sans', 'sans-serif',
    ].join(','),
    color: colors.neutrals['text-strong'].value,
    fontSize: 14,
    fontWeight: 400,
    textTransform: 'uppercase',
  },
  h4: {
    fontSize: 20,
    color: colors.neutrals['text-strong'].value,
    letterSpacing: 0.15,
    fontWeight: 500,
    textTransform: 'uppercase',
  },
  h5: {
    fontFamily: [
      'Noto Sans', 'ArialMT', 'Arial', 'sans-serif',
    ].join(','),
    color: colors.neutrals['text-strong'].value,
    textTransform: 'uppercase',
  },
  h6: {
    fontFamily: [
      'Noto Sans Bold', 'OpenSans-Bold', 'Open Sans Bold', 'Open Sans', 'sans-serif',
    ].join(','),
    color: colors.neutrals['text-strong'].value,
    fontSize: 13,
    fontWeight: 700,
  },
  body1: {
    color: colors.neutrals['text-strong'].value,
  },
  body2: {
    fontSize: 14,
    color: colors.neutrals['text-strong'].value,
    fontWeight: 400,
  },
  body3: {
    fontSize: 12,
    color: colors.neutrals['text-strong'].value,
    display: 'block',
  },
  subtitle1: {
    fontSize: 11,
    color: colors.neutrals['text-strong'].value,
  },
  subtitle2: {
    fontSize: 13,
  },
});