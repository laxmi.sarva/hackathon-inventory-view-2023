import Chip from '@mui/material/Chip';

function Method() {
  return <Chip label="GET" className="method" />;
}

export default Method;