import Chip from '@mui/material/Chip';
import ErrorIcon from '@mui/icons-material/Error';
import ReportProblemIcon from '@mui/icons-material/ReportProblem';
import InfoIcon from '@mui/icons-material/Info';

function RiskScore() {
  return <Chip icon={<InfoIcon />} label="10" className="risk low" />   ;
/*high risk icon <ErrorIcon />*/
/*medium risk icon <ReportProblemIcon />>*/
/*low risk icon <InfoIcon />*/
}

export default RiskScore;