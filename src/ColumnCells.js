import { ThemeContext } from "./ThemeContext"
import { styled } from '@mui/material';
import  React, {useContext} from "react";
import { Chip } from '@mui/material';
import ErrorIcon from '@mui/icons-material/Error';
import ReportProblemIcon from '@mui/icons-material/ReportProblem';
import InfoIcon from '@mui/icons-material/Info';
import TrafficIcon from '@mui/icons-material/Traffic';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import AssuredWorkloadIcon from '@mui/icons-material/AssuredWorkload'
import shadowIcon from './shadow.svg'



const StyledChip = styled(Chip)(({type, colors}) => ({
  border: `1px solid ${colors[type]?.border?.value}`,
  borderRadius: '4px',
  height: '24px',
  minWidth: '78px',
  background: colors[type]['background-strong']?.value,
  color: colors.neutrals['text-strong'].value,
  '& .MuiChip-icon': {
      color: colors[type]?.icon?.value,
      height: '16.67px',
      width: '16.67px'
  }
}))
export const GetRiskScore = (params) => {
  const themeContextVal = useContext(ThemeContext)
  let risk = "warning";
  if(params.value === "low"){
    risk = 'neutrals'
  }
  if(params.value === "high"){
    risk= 'danger'
  }
  return <StyledChip
            colors={themeContextVal}
            type={risk}
            icon={params.value === 'low' ? <InfoIcon/> : params.value === 'high' ? <ErrorIcon/> : <ReportProblemIcon/>} label={params.value} />
} 

export const Observations = (params) => {
  const themeContextVal = useContext(ThemeContext)
  const [circle, storage, bright, vis] = params.value
  const strongColor = themeContextVal.danger['icon'].value
  const neutralColor = themeContextVal.neutrals['background-strong'].value
  const borderMedium = themeContextVal.neutrals['border-medium'].value
  const success = themeContextVal.success['icon'].value
  return (
    <>
      <TrafficIcon sx={{color: circle === '1' ? success : neutralColor}} style={{borderRight:'1px solid', borderColor: borderMedium, paddingRight:'8px', marginRight:'12px'}} />
      <img alt="" src={shadowIcon} style={{color: storage === '1' ? strongColor : neutralColor}}  />
      <AssuredWorkloadIcon sx={{color: bright === '1' ? strongColor : neutralColor}}  />
      <LockOpenIcon sx={{color: vis === '1' ? strongColor : neutralColor}}  />
    </>
  )
}

export const Method = (params) => {
  const themeContextVal = useContext(ThemeContext)
  console.log(params)
  let colorVal
  if(params.value === 'Get'){
    colorVal = themeContextVal.neutrals['background-muted'].value
  }
  if(params.value === 'Delete'){
    colorVal = themeContextVal.neutrals['background-muted'].value
  }
  if(params.value === 'Put'){
    colorVal = themeContextVal.neutrals['background-muted'].value
  }
  if(params.value === 'Post'){
    colorVal = themeContextVal.neutrals['background-muted'].value
  }
  return (
    <Chip
      label={params.value}
      sx={{
        backgroundColor: colorVal,
        clipPath: 'polygon(0% 0%, 85% 0%, 100% 50%, 85% 100%, 0% 100%)',
        width: '60px',
        height: '20px',
        fontSize: '8px',
        fontWeight: '600',
        color: themeContextVal.neutrals['text-strong'].value
      }}
    />
  )
}