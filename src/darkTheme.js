export const darkTheme = {
  "neutrals": {
    "background-inverted": {
      "type": "color",
      "value": "#c9c9c9"
    },
    "text-inverted": {
      "type": "color",
      "value": "#1e1e1e"
    },
    "icon-strong": {
      "type": "color",
      "value": "#555555"
    },
    "background-muted": {
      "type": "color",
      "value": "#202020"
    },
    "background-pressed": {
      "type": "color",
      "value": "rgba(249, 249, 249, 0.1200)"
    },
    "border-medium": {
      "type": "color",
      "value": "#414141"
    },
    "border-weak": {
      "type": "color",
      "value": "#333333"
    },
    "border-strong": {
      "type": "color",
      "value": "#555555"
    },
    "icon": {
      "type": "color",
      "value": "#4d4d4d"
    },
    "background-selected": {
      "type": "color",
      "value": "rgba(0, 187, 211, 0.1200)"
    },
    "background-hover": {
      "type": "color",
      "value": "rgba(249, 249, 249, 0.0300)"
    },
    "background-strong": {
      "type": "color",
      "value": "#333333"
    },
    "background": {
      "type": "color",
      "value": "#242424"
    },
    "text-medium": {
      "type": "color",
      "value": "#cccccc"
    },
    "background-medium": {
      "type": "color",
      "value": "#2a2a2a"
    },
    "text-disabled": {
      "type": "color",
      "value": "#6b6b6b"
    },
    "text-weak": {
      "type": "color",
      "value": "#ababab"
    },
    "icon-hover": {
      "type": "color",
      "value": "#7f7f7f"
    },
    "text-strong": {
      "type": "color",
      "value": "#f2f2f2"
    }
  },
  "danger": {
    "background-highlight": {
      "type": "color",
      "value": "rgba(249, 0, 0, 0.2000)"
    },
    "background-strong": {
      "type": "color",
      "value": "#74191c"
    },
    "background-weak": {
      "type": "color",
      "value": "#611417"
    },
    "text": {
      "type": "color",
      "value": "#f3d4d5"
    },
    "icon": {
      "type": "color",
      "value": "#da7f82"
    },
    "border": {
      "type": "color",
      "value": "#c73e43"
    }
  },
  "selected": {
    "link": {
      "type": "color",
      "value": "#33c9dc"
    },
    "background-hover": {
      "type": "color",
      "value": "rgba(30, 30, 30, 0.0800)"
    },
    "background": {
      "type": "color",
      "value": "#00bbd3"
    },
    "icon": {
      "type": "color",
      "value": "#33c9dc"
    },
    "text": {
      "type": "color",
      "value": "#1e1e1e"
    },
    "background-pressed": {
      "type": "color",
      "value": "rgba(30, 30, 30, 0.1600)"
    }
  },
  "warning": {
    "background-weak": {
      "type": "color",
      "value": "#482902"
    },
    "border": {
      "type": "color",
      "value": "#c16d04"
    },
    "icon": {
      "type": "color",
      "value": "#f5ac50"
    },
    "background-strong": {
      "type": "color",
      "value": "#603602"
    },
    "text": {
      "type": "color",
      "value": "#f9cf9b"
    }
  },
  "success": {
    "border": {
      "type": "color",
      "value": "#339a6d"
    },
    "background-strong": {
      "type": "color",
      "value": "#004124"
    },
    "icon": {
      "type": "color",
      "value": "#66b391"
    },
    "background-weak": {
      "type": "color",
      "value": "#00341d"
    },
    "text": {
      "type": "color",
      "value": "#cce6da"
    }
  }
}