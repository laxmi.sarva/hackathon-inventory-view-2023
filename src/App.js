import { useState } from 'react';
import { styled } from '@mui/material';
import { DataGridPro } from '@mui/x-data-grid-pro';
import { LicenseInfo } from '@mui/x-license-pro';
import { rowsData } from './rows';
import { lightTheme } from './lightTheme';
import { darkTheme } from './darkTheme';
import Switch from '@mui/material/Switch';
import Typography from '@mui/material/Typography';
import { getTheme } from './theme';
import { ThemeProvider } from '@mui/material/styles';
import { ThemeContext } from "./ThemeContext"
import CqGlobalStyles from './CqGlobalStyles';
import { GetRiskScore, Observations, Method } from './ColumnCells';
import LightModeIcon from '@mui/icons-material/LightMode';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import InsetList from './Navigation.js';
import lightLogo from './logo-lightmode.svg';
import darkLogo from './logo-darkmode.svg';
LicenseInfo.setLicenseKey("0283fbefd408d996b365ba9c706f1643Tz02MjIyOCxFPTE3MTA3Njc1NTY0NjgsUz1wcm8sTE09c3Vic2NyaXB0aW9uLEtWPTI=");

const StyledContainer = styled('div')(({colors}) => ({
  backgroundColor: colors.neutrals['background'].value,
}))

const StyledSwitch = styled(Switch)(({colors, checked}) => ({
  '& .Mui-checked': {
    color: colors['neutrals']['text-medium'].value,
  },
  '& .MuiSwitch-track': {
    backgroundColor: checked ? '#fff !important' : 'transparent'
  }
}))

function App() {
  const [ theme, setTheme ] = useState('dark')
  let i = 0;
  const rows = rowsData.map((row) => {
    row.id = i;
    i++
    return row
  });
  const columns = [];
  columns.push(
    {field: 'observations',  headerName: 'Observations',  minWidth: 150, maxWidth: 300, sortable: false, filterable: false, renderCell: Observations},
    {field: 'riskScore',  headerName: 'Risk Score',  minWidth: 150, maxWidth: 300, sortable: false, filterable: false, renderCell: GetRiskScore},
    {field: 'method',  headerName: 'Method',  minWidth: 150, maxWidth: 300, sortable: false, filterable: false, renderCell: Method},
    {field: 'host',  headerName: 'HOST',  minWidth: 150, maxWidth: 300, sortable: false, filterable: false},
    {field: 'firstDiscovered',  headerName: 'First Discovered',  minWidth: 150, maxWidth: 300, sortable: false, filterable: false},
    {field: 'lastObserved',  headerName: 'Last Observed',  minWidth: 150, maxWidth: 300, sortable: false, filterable: false},
    {field: 'auth',  headerName: 'Auth',  minWidth: 150, maxWidth: 300, sortable: false, filterable: false},
    {field: 'transactions',  headerName: 'Transactions',  minWidth: 150, maxWidth: 300, sortable: false, filterable: false},
    {field: 'clientError',  headerName: 'Client Error',  minWidth: 150, maxWidth: 300, sortable: false, filterable: false},
    {field: 'serverError',  headerName: 'Server Error',  minWidth: 150, maxWidth: 300, sortable: false, filterable: false},
  )
  const colors = theme === 'dark' ? darkTheme : lightTheme
  const handleChange = () => {
    setTheme(val => val === 'light' ? 'dark' : 'light');
  };
  
  const themeVal = getTheme(colors);
  return (
    <ThemeProvider theme={themeVal}>
      <ThemeContext.Provider value={colors}>
      <CqGlobalStyles colors={colors} />
      <StyledContainer colors={colors}>
        <div className="branding">
          <div><img style={{width: '200px', height: '42px'}} alt="" src={theme === 'light' ? lightLogo : darkLogo} /></div>
          <div style={{height: '40px',display: 'flex',justifyContent: 'end', width: '100%'}}>
            <StyledSwitch
              checked={theme === 'light'}
              onChange={handleChange}
              colors={colors}
              icon={<LightModeIcon/>}
              checkedIcon={<DarkModeIcon/>}
            />
          </div>
        </div>
        <div className='page-container'>
          <div className="nav-wrapper invisible-scrollbar">
            <InsetList sx={{ width: '100%', maxWidth: 360, backgroundcolor: colors.neutrals['background-medium'].value, }} aria-label="contacts"></InsetList>
          </div>

          
          <div>
            <div style={{height: '40px', marginLeft: "10px"}}><Typography variant="h4">API Inventory</Typography></div>
            <DataGridPro
            rows={rows}
            columns={columns}
            sx={{
              border: 1.5,
              borderColor: colors.neutrals['border-medium'].value,
              width: '100%',
              backgroundcolor: colors.neutrals['background-medium'].value,
          

              
              '& .MuiDataGrid-withBorderColor': {
                borderBottom: 1.5,
                borderColor: colors.neutrals['border-weak'].value,
            },
            
              '& .MuiDataGrid-row': {
                  backgroundColor: colors.neutrals['background-medium'].value,
                  cursor: 'pointer',
              },
              '& .MuiDataGrid-row:hover': {
                backgroundColor: colors.neutrals['background-hover'].value,
            },
              
              '& .MuiDataGrid-columnHeader': {
                backgroundColor: colors.neutrals['background'].value,
                // fontWeight: 800
            },
              '& .MuiDataGrid-columnHeaderTitle': {
                  fontWeight: '400',
              },
            
            
              '& .MuiDataGrid-row.Mui-selected': {
                backgroundColor: colors.neutrals['background-selected'].value,
              },
              '& .MuiDataGrid-row.Mui-selected:hover': {
                backgroundColor: colors.neutrals['background-selected'].value,
              },
              
            
              '& .MuiDataGrid-cell.MuiDataGrid-withBorderColor': {
                  borderColor: colors.neutrals['border-weak'].value
              },
              '& .MuiDataGrid-columnHeader.MuiDataGrid-withBorderColor': {
                  borderColor: colors.neutrals['border-weak'].value
              },
              '& .MuiDataGrid-columnSeparator': {
                  visibility: 'visible',
                  color: colors.neutrals['border-medium'].value,
              },
              '& .MuiDataGrid-columnSeparator:hover': {
                visibility: 'visible',
                color: colors.neutrals['border-strong'].value,
            },
              '& .MuiDataGrid-columnHeader:last-of-type .MuiDataGrid-columnSeparator': {
                  visibility: 'hidden',
              },
              '& .MuiDataGrid-footerContainer.MuiDataGrid-withBorderColor': {
                  border: 'none',
              },
              '& .MuiTablePagination-select': {
                  paddingTop: '8px',
              },
              '& .MuiDataGrid-cellContent': {
                color: colors.neutrals['text-medium'].value,
              }
          }} />
          </div>
        </div>
      </StyledContainer>
      </ThemeContext.Provider>
    </ThemeProvider>
  );
}

export default App;
