import * as React from 'react';
import { styled } from '@mui/material';
import { ThemeContext } from "./ThemeContext";
import {useContext} from "react";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';

const StyledContainer = styled('NavItemActive')(({colors}) => ({
    backgroundColor: colors.neutrals['background-selected'].value,
    
  }))

export default function InsetList() {
    const themeContextVal = useContext(ThemeContext)
    const NavSelected = themeContextVal.neutrals['background-selected'].value
    const NavBackground = themeContextVal.neutrals['background-muted'].value
  return (
    <List
      sx={{ width: '100%', maxWidth: 360 }}
      aria-label="contacts"
      style={{background: NavBackground, borderRadius: '4px'}}
    >
      <ListItem className="NavItemHeader">
          <ListItemText primary="Runtime Inventory"/>
      </ListItem>        
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="API Summary Dashboard" />
        </ListItemButton>
      </ListItem>
      <ListItem disablePadding style={{background: NavSelected}}>
        <ListItemButton>
          <ListItemText primary="API Inventory" />
        </ListItemButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Sensitive Data Exposure" />
        </ListItemButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="API Risk Categories" />
        </ListItemButton>
      </ListItem>      
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Inventory Settings" />
        </ListItemButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Sensitive Data Expressions" />
        </ListItemButton>
      </ListItem>
      <ListItem className="NavItemHeader">
          <ListItemText primary="Threat Protection"/>
      </ListItem>              
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Sitemap Discovery" />
        </ListItemButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Detection" />
        </ListItemButton>
      </ListItem>      
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Mitigation" />
        </ListItemButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Automation Indicators" />
        </ListItemButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Fraud Indicators" />
        </ListItemButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Mitigation Policies" />
        </ListItemButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Fingerprint Policies" />
        </ListItemButton>
      </ListItem>
      <ListItem className="NavItemHeader">
          <ListItemText primary="API Security Testing"/>
      </ListItem>         
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Test Plans" />
        </ListItemButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Test Runs" />
        </ListItemButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Test Catalog" />
        </ListItemButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Sources" />
        </ListItemButton>
      </ListItem>              
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Integrations" />
        </ListItemButton>
      </ListItem>
      <ListItem className="NavItemHeader">
          <ListItemText primary="Applications"/>
      </ListItem>        
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Manage Applications" />
        </ListItemButton>
      </ListItem>                  
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="API Definitions" />
        </ListItemButton>
      </ListItem>                  
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Real IP Extraction" />
        </ListItemButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="App Tags" />
        </ListItemButton>
      </ListItem>              
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Auth Expressions" />
        </ListItemButton>
      </ListItem>                  
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Data Extractions" />
        </ListItemButton>
      </ListItem>                  
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="HTTP Traffic Filters" />
        </ListItemButton>
      </ListItem>                  
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Session Tracking" />
        </ListItemButton>
      </ListItem>
      <ListItem className="NavItemHeader">
          <ListItemText primary="Events"/>
      </ListItem>
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Audit Logs" />
        </ListItemButton>
      </ListItem>              
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Transactions" />
        </ListItemButton>
      </ListItem>
      <ListItem className="NavItemHeader">
          <ListItemText primary="Integrations"/>
      </ListItem>                  
      <ListItem disablePadding>
        <ListItemButton>
          <ListItemText primary="Data Export" />
        </ListItemButton>
      </ListItem>                                                                   
    </List>
  );
}