import { createTheme } from '@mui/material/styles';
import { getTypography } from './getTypography';

const themeOptions = (colors) => ({
  typography: getTypography(colors),
  drawerWidth: 245,
  breakpoints: {
      values: {
          xs: 0,
          sm: 600,
          md: 1250,
          lg: 1450,
          xl: 1700,
      },
  },
});


export const getTheme = (colors) => createTheme(themeOptions(colors));
