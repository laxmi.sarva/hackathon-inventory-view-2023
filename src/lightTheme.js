export const lightTheme = {
  "neutrals": {
    "background-inverted": {
      "type": "color",
      "value": "#414141"
    },
    "text-inverted": {
      "type": "color",
      "value": "#f2f2f2"
    },
    "icon-strong": {
      "type": "color",
      "value": "#bbbbbb"
    },
    "background-muted": {
      "type": "color",
      "value": "#f2f2f2"
    },
    "background-pressed": {
      "type": "color",
      "value": "rgba(30, 30, 30, 0.1200)"
    },
    "border-medium": {
      "type": "color",
      "value": "#dfdfdf"
    },
    "border-weak": {
      "type": "color",
      "value": "#f2f2f2"
    },
    "border-strong": {
      "type": "color",
      "value": "#cccccc"
    },
    "icon": {
      "type": "color",
      "value": "#cccccc"
    },
    "background-selected": {
      "type": "color",
      "value": "rgba(0, 112, 127, 0.1200)"
    },
    "background-hover": {
      "type": "color",
      "value": "rgba(30, 30, 30, 0.0300)"
    },
    "background-strong": {
      "type": "color",
      "value": "#dfdfdf"
    },
    "background": {
      "type": "color",
      "value": "#f9f9f9"
    },
    "text-medium": {
      "type": "color",
      "value": "#444444"
    },
    "background-medium": {
      "type": "color",
      "value": "#ffffff"
    },
    "text-disabled": {
      "type": "color",
      "value": "#c9c9c9"
    },
    "text-weak": {
      "type": "color",
      "value": "#7f7f7f"
    },
    "icon-hover": {
      "type": "color",
      "value": "#aaaaaa"
    },
    "text-strong": {
      "type": "color",
      "value": "#1e1e1e"
    }
  },
  "danger": {
    "background-highlight": {
      "type": "color",
      "value": "rgba(249, 0, 0, 0.0800)"
    },
    "background-strong": {
      "type": "color",
      "value": "#f3d4d5"
    },
    "background-weak": {
      "type": "color",
      "value": "#f9eaea"
    },
    "text": {
      "type": "color",
      "value": "#911f23"
    },
    "icon": {
      "type": "color",
      "value": "#cd5458"
    },
    "border": {
      "type": "color",
      "value": "#ecbfc0"
    }
  },
  "selected": {
    "link": {
      "type": "color",
      "value": "#00707f"
    },
    "background-hover": {
      "type": "color",
      "value": "rgba(242, 242, 242, 0.0800)"
    },
    "background": {
      "type": "color",
      "value": "#00707f"
    },
    "icon": {
      "type": "color",
      "value": "#0096a9"
    },
    "text": {
      "type": "color",
      "value": "#f2f2f2"
    },
    "background-pressed": {
      "type": "color",
      "value": "rgba(242, 242, 242, 0.1600)"
    }
  },
  "warning": {
    "background-weak": {
      "type": "color",
      "value": "#fef3e6"
    },
    "border": {
      "type": "color",
      "value": "#fbdbb4"
    },
    "icon": {
      "type": "color",
      "value": "#f7b869"
    },
    "background-strong": {
      "type": "color",
      "value": "#fce7cd"
    },
    "text": {
      "type": "color",
      "value": "#794403"
    }
  },
  "success": {
    "border": {
      "type": "color",
      "value": "#80c0a4"
    },
    "background-strong": {
      "type": "color",
      "value": "#cce6da"
    },
    "icon": {
      "type": "color",
      "value": "#4da77f"
    },
    "background-weak": {
      "type": "color",
      "value": "#e6f2ed"
    },
    "text": {
      "type": "color",
      "value": "#006136"
    }
  }
}